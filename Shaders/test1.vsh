//attribute highp vec4 qt_Vertex;
//attribute highp vec4 qt_MultiTexCoord0;
//uniform highp mat4 qt_ModelViewProjectionMatrix;
//varying highp vec4 qt_TexCoord0;

//void main(void)
//{
//    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;
//    qt_TexCoord0 = qt_MultiTexCoord0;
//}

#version 330

layout (location = 0) in vec3 pos;
// we have not define variables later in code
// vec3 means 3d vector like x,y,z pose

void main(){
    gl_Position = vec4(pos, 1.0); // needs vec4 to pass
}
