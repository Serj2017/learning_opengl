TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Mesh.cpp \
    Shader.cpp \
    Window.cpp \
    Camera.cpp \
    Texture.cpp \
    Light.cpp \
    Material.cpp \
    DirectionalLight.cpp \
    PointLight.cpp

LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lGLEW -lglfw -lGL

INCLUDEPATH += /usr/lib/x86_64-linux-gnu
DEPENDPATH += /usr/lib/x86_64-linux-gnu

DISTFILES += \
    README.md \
    sketch.txt \
    Shaders/shader.fsh \
    Shaders/shader.vsh

HEADERS += \
    Mesh.h \
    Shader.h \
    Window.h \
    Camera.h \
    stb_image.h \
    Texture.h \
    Light.h \
    Material.h \
    DirectionalLight.h \
    PointLight.h \
    commonvalues.h
